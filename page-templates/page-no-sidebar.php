<?php
/**
 * The no sidebar page template file
 *
 * @package weblogs
 *
 * Template Name: No Sidebar
 */

get_header();
?>

<header class="page__header">
	<div class="container">
		<h1 class="page__title">
			<?php the_title(); ?>
		</h1>
	</div>
</header>
<div class="container page-full u-space">
	<div class="row">
		<div class="col-xs-12 col-md-12">
			<?php if ( '' !== get_the_post_thumbnail() ) { ?>
			<div class="page_thumbnail">
				<?php the_post_thumbnail( 'large' ); ?>
			</div>
			<?php } ?>
			<main role="main">
			<?php get_template_part( 'loop', 'page' ); ?>
			</main>
		</div>
	</div>
</div>
<?php
get_footer();
