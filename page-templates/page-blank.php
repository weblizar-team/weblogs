<?php
/**
 * The blank page template file
 *
 * @package weblogs
 *
 * Template Name: Blank Page
 */

get_header();

while ( have_posts() ) {
	the_post();
	?>
	<article>
	<?php the_content(); ?>		
	</article>
	<?php
}
get_footer();
