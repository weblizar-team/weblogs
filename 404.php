<?php
/**
 * The 404 template file
 *
 * @package weblogs
 */

get_header();
?>

<div class="container page-404 u-space">
	<div class="row">
		<div class="col-xs-12 col-md-12 text-center">
			<main role="main">
				<h1><?php esc_html_e( '404 Error', 'weblogs' ); ?></h1>
				<h3><?php esc_html_e( 'Nothing found here, maybe you can try to search?', 'weblogs' ); ?></h3>
				<div class="page-404-search"><?php get_search_form(); ?></div>
				<h4>
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>"> <i class="fas fa-arrow-circle-left"></i> <?php esc_html_e( 'Back to Home', 'weblogs' ); ?></a>
				</h4>
			</main>
		</div>
	</div>
</div>
<?php
get_footer();
