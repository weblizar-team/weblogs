<?php
/**
 * Functions and definitions
 *
 * @package weblogs
 */

// Require classes.
require_once get_template_directory() . '/lib/classes/class-weblogs-walker-main-nav.php';

// Theme customizer.
require_once get_template_directory() . '/lib/customize.php';

// Helper functions.
require_once get_template_directory() . '/lib/helpers.php';

// Enqueue assets.
require_once get_template_directory() . '/lib/enqueue-assets.php';

// Register sidebars.
require_once get_template_directory() . '/lib/sidebars.php';

// Theme support.
require_once get_template_directory() . '/lib/theme-support.php';

// Navigation menu.
require_once get_template_directory() . '/lib/navigation.php';

// Comment template.
require_once get_template_directory() . '/lib/comment-callback.php';

// Required Plugin.
require_once get_template_directory() . '/lib/include-plugins.php';

if ( ! isset( $content_width ) ) {
	$content_width = 800;
}

/**
 * Content width adjustment
 *
 * @return void
 */
function weblogs_content_width() {
	global $content_width;
	global $post;

	if ( is_single() && 'post' === $post->post_type ) {
		$sidebar         = weblogs_get_post_meta( $post->ID, '_weblogs_post_sidebar', 'no-sidebar' );
		$primary_sidebar = is_active_sidebar( 'primary-sidebar' );

		$valid = array( 'no-sidebar', 'right-sidebar', 'left-sidebar' );
		if ( ! in_array( $sidebar, $valid, true ) ) {
			$sidebar = 'no-sidebar';
		}

		$has_right_sidebar = false;
		$has_left_sidebar  = false;
		$no_sidebar        = true;
		if ( 'right-sidebar' === $sidebar ) {
			$has_right_sidebar = true;
		} elseif ( 'left-sidebar' === $sidebar ) {
			$has_left_sidebar = true;
		}

		if ( $primary_sidebar && ( $has_right_sidebar || $has_left_sidebar ) ) {
			$no_sidebar = false;
		}

		$content_width = $no_sidebar ? 800 : 730;
	}
}
add_action( 'template_redirect', 'weblogs_content_width' );

/**
 * Load theme textdomain
 *
 * @return void
 */
function weblogs_load_textdomain() {
	load_theme_textdomain( 'weblogs', get_template_directory() . '/languages' );
}
add_action( 'after_setup_theme', 'weblogs_load_textdomain' );

