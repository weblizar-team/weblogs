<?php
/**
 * The single post template file
 *
 * @package weblogs
 */

get_header();

$sidebar         = weblogs_get_post_meta( get_the_ID(), '_weblogs_post_sidebar', 'no-sidebar' );
$primary_sidebar = is_active_sidebar( 'primary-sidebar' );

$valid = array( 'no-sidebar', 'right-sidebar', 'left-sidebar' );
if ( ! in_array( $sidebar, $valid, true ) ) {
	$sidebar = 'no-sidebar';
}

$has_right_sidebar = false;
$has_left_sidebar  = false;
$no_sidebar        = true;
if ( 'right-sidebar' === $sidebar ) {
	$has_right_sidebar = true;
} elseif ( 'left-sidebar' === $sidebar ) {
	$has_left_sidebar = true;
}

$signle_post_class = 'post-single-full';
$main_columns      = '12';
if ( $primary_sidebar && ( $has_right_sidebar || $has_left_sidebar ) ) {
	$no_sidebar = false;
	$signle_post_class = 'post-single-sidebar';
	$main_columns = '8';
}
?>

<div class="container post-single <?php echo esc_attr( $signle_post_class ); ?> u-space">
	<div class="row">
		<?php if ( $has_left_sidebar && $primary_sidebar ) { ?>
			<div class="col-xs-12 col-md-4">
				<?php get_sidebar(); ?>
			</div>
		<?php } ?>

		<div class="col-xs-12 col-md-<?php echo esc_attr( $main_columns ); ?>">
			<main role="main">
			<?php get_template_part( 'loop', 'single' ); ?>
			</main>
		</div>

		<?php if ( $has_right_sidebar && $primary_sidebar ) { ?>
			<div class="col-xs-12 col-md-4">
				<?php get_sidebar(); ?>
			</div>
		<?php } ?>
	</div>
</div>
<?php
get_footer();
