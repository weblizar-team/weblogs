<?php
/**
 * The search form template file
 *
 * @package weblogs
 */

?>
<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<label class="search-form__label">
		<span class="screen-reader-text"><?php echo esc_html_x( 'Search for:', 'label', 'weblogs' ); ?></span>
		<input type="search" class="search-form__field" placeholder="<?php echo esc_attr_x( 'Search &hellip;', 'placeholder', 'weblogs' ); ?>" value="<?php echo esc_attr( get_search_query() ); ?>" name="s" />
	</label>
	<button class="search-form__button" type="submit">
		<span class="screen-reader-text"><?php echo esc_html_x( 'Search', 'submit button', 'weblogs' ); ?></span><i class="fas fa-search" aria-hidden="true"></i>
	</button>
</form>
