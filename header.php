<?php
/**
 * The header template file
 *
 * @package weblogs
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

	<header class="main-header" role="banner">
		<div class="header container">
			<nav class="header-nav navbar navbar-expand-md navbar-light bg-faded" role="navigation" aria-label="<?php esc_attr_e( 'Main Navigation', 'weblogs' ); ?>">
				<span class="header__logo">
				<?php
				if ( has_custom_logo() ) {
					the_custom_logo();
				} 
				if (display_header_text()==true){ ?> 
						  	<a class="navbar-brand" href="<?php echo esc_url(home_url( '/' )); ?>"><h1><?php echo esc_html(get_bloginfo('name'));  ?></h1></a>
						<?php  } ?>
							
			    <?php if (display_header_text()==true){ ?>
							<p><?php bloginfo('description'); ?></p> 
						<?php } ?>
				</span>

				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="bs4navbar" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<?php
				if ( has_nav_menu( 'main-menu' ) ) {
					wp_nav_menu(
						array(
							'theme_location'  => 'main-menu',
							'container'       => 'div',
							'container_id'    => 'main-menu',
							'container_class' => 'collapse navbar-collapse',
							'menu_id'         => false,
							'menu_class'      => 'navbar-nav mx-auto',
							'depth'           => 3,
							'walker'          => new WeBlogs_Walker_Main_Nav(),
						)
					);
				}
				?>
			</nav>
		</div>
	</header>

	<div id="content">
