<?php
/**
 * The post content template part
 *
 * @package weblogs
 */

?>
<article <?php post_class(); ?>>
	<div class="post__inner">
		<?php if ( '' !== get_the_post_thumbnail() ) { ?>
		<div class="post__thumbnail">
			<?php the_post_thumbnail( 'large' ); ?>
		</div>
		<?php } ?>

		<?php get_template_part( 'template-parts/post/header' ); ?>

		<?php if ( is_single() ) { ?>
		<div class="post__content">
			<?php
			the_content();
			wp_link_pages();
			?>
		</div>
		<?php } else { ?>
		<div class="post__excerpt">
			<?php the_excerpt(); ?>
		</div>
		<?php } ?>

		<?php if ( ! is_single() ) { ?>
		<div class="post__readmore">
			<?php weblogs_readmore_link(); ?>
		</div>
		<?php } ?>

		<?php if ( is_single() ) { ?>
			<?php get_template_part( 'template-parts/post/footer' ); ?>
		<?php } ?>
	</div>
</article>
