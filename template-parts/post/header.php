<?php
/**
 * The post header template part
 *
 * @package weblogs
 */

?>
<header class="post__header">
	<?php if ( is_single() ) { ?>
	<h1 class="post__single-title">
		<?php the_title(); ?>
	</h1>
	<?php } else { ?>
	<h2 class="post__title">
		<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
	</h2>
	<?php } ?>

	<div class="post__meta">
		<?php weblogs_post_meta(); ?>
	</div>
</header>
