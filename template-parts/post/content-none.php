<?php
/**
 * The post content none template part
 *
 * @package weblogs
 */

?>

<h3><?php esc_html_e( 'Sorry, no posts matched your criteria.', 'weblogs' ); ?></h3>
<div class="page-404-search"><?php get_search_form(); ?></div>
<h4>
	<a href="<?php echo esc_url( home_url( '/' ) ); ?>"> <i class="fas fa-arrow-circle-left"></i> <?php esc_html_e( 'Back to Home', 'weblogs' ); ?></a>
</h4>
