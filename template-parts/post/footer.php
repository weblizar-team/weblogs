<?php
/**
 * The post footer template part
 *
 * @package weblogs
 */

?>
<footer class="post__footer">
	<?php
	if ( has_category() ) {
		echo '<div class="post__cats">';
		/* translators: used between categories, there is a space after the comma */
		$cats_list = get_the_category_list( esc_html__( ' ', 'weblogs' ) );
		printf(
			/* translators: %s: category list */
			wp_kses(
				__(
					'<span class="post__posted_in">Posted in </span> %s',
					'weblogs'
				),
				array(
					'span' => array(
						'class' => array(),
					),
				)
			),
			wp_kses(
				$cats_list,
				array(
					'a' => array(
						'rel'  => array(),
						'href' => array(),
					),
				)
			)
		);
		echo '</div>';
	}
	if ( has_tag() ) {
		echo '<div class="post__tags">';
		$tags_list = get_the_tag_list( '<ul><li>', '</li><li> ', '</li></ul>' );
		/* translators: %s: tag list */
		printf(
			'%s',
			wp_kses(
				$tags_list,
				array(
					'ul' => array(),
					'li' => array(),
					'a'  => array(
						'rel'  => array(),
						'href' => array(),
					),
				)
			)
		);
		echo '</div>';
	}
	?>
</footer>
