<?php
/**
 * The post content audio template part
 *
 * @package weblogs
 */

$content = apply_filters( 'the_content', get_the_content() );
$audios  = get_media_embedded_in_content( $content, array( 'audio', 'iframe' ) );
?>
<article <?php post_class(); ?>>
	<div class="post__inner">
		<?php if ( get_the_post_thumbnail() !== '' && ( empty( $audios ) || is_single() ) ) { ?>
		<div class="post__thumbnail">
			<?php the_post_thumbnail( 'large' ); ?>
		</div>
		<?php } ?>

		<?php if ( ! is_single() && ! empty( $audios ) ) { ?>
			<div class="post__audio">
				<?php printf( '%s', $audios[0] ); ?>
			</div>
		<?php } ?>

		<?php get_template_part( 'template-parts/post/header' ); ?>

		<?php if ( is_single() ) { ?>
		<div class="post__content post__content--audio">
			<?php
			the_content();
			wp_link_pages();
			?>
		</div>
		<?php } else { ?>
		<div class="post__excerpt">
			<?php the_excerpt(); ?>
		</div>
		<?php } ?>

		<?php if ( ! is_single() ) { ?>
		<div class="post__readmore">
			<?php weblogs_readmore_link(); ?>
		</div>
		<?php } ?>

		<?php if ( is_single() ) { ?>
			<?php get_template_part( 'template-parts/post/footer' ); ?>
		<?php } ?>
	</div>
</article>
