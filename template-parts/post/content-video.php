<?php
/**
 * The post content video template part
 *
 * @package weblogs
 */

$content = apply_filters( 'the_content', get_the_content() );
$videos  = get_media_embedded_in_content( $content, array( 'video', 'object', 'embed', 'iframe' ) );
?>
<article <?php post_class(); ?>>
	<div class="post__inner">
		<?php if ( get_the_post_thumbnail() !== '' && ( empty( $videos ) || is_single() ) ) { ?>
		<div class="post__thumbnail">
			<?php the_post_thumbnail( 'large' ); ?>
		</div>
		<?php } ?>

		<?php if ( ! is_single() && ! empty( $videos ) ) { ?>
			<div class="post__video">
				<?php if ( strpos( $videos[0], '<iframe' ) !== false ) { ?>
				<div class="responsive-video">
				<?php } ?>
				<?php printf( '%s', $videos[0] ); ?>
				<?php if ( strpos( $videos[0], '<iframe' ) !== false ) { ?>
				</div>
				<?php } ?>
			</div>
		<?php } ?>

		<?php get_template_part( 'template-parts/post/header' ); ?>

		<?php if ( is_single() ) { ?>
		<div class="post__content post__content--video">
			<?php
			the_content();
			wp_link_pages();
			?>
		</div>
		<?php } else { ?>
		<div class="post__excerpt">
			<?php the_excerpt(); ?>
		</div>
		<?php } ?>

		<?php if ( ! is_single() ) { ?>
		<div class="post__readmore">
			<?php weblogs_readmore_link(); ?>
		</div>
		<?php } ?>

		<?php if ( is_single() ) { ?>
			<?php get_template_part( 'template-parts/post/footer' ); ?>
		<?php } ?>
	</div>
</article>
