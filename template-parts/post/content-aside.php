<?php
/**
 * The post content aside template part
 *
 * @package weblogs
 */

?>
<article <?php post_class(); ?>>
	<div class="post__inner">
		<div class="post__content post__content--aside">
			<?php the_content(); ?>
		</div>

		<div class="post__meta">
			<?php weblogs_post_meta(); ?>
		</div>

		<?php if ( is_single() ) { ?>
			<?php get_template_part( 'template-parts/post/footer' ); ?>
		<?php } ?>
	</div>
</article>
