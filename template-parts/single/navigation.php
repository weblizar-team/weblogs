<?php
/**
 * The post navigation template part
 *
 * @package weblogs
 */

$prev = get_previous_post();
$next = get_next_post();

if ( $prev || $next ) { ?>
<nav class="post-navigation u-space-b-60" role="navigation">
	<h2 class="screen-reader-text"><?php esc_attr_e( 'Post Navigation', 'weblogs' ); ?></h2>
	<div class="post-navigation__links">
		<?php if ( $prev ) { ?>
		<div class="post-navigation__post post-navigation__post--prev">
			<a class="post-navigation__link" href="<?php the_permalink( $prev->ID ); ?>">
				<?php if ( has_post_thumbnail( $prev->ID ) ) { ?>
				<div class="post-navigation__thumbnail">
					<?php echo get_the_post_thumbnail( $prev->ID, 'thumbnail' ); ?>
				</div>
				<?php } ?>
				<div class="post-navigation__content">
					<span class="post-navigation__title">
						<?php esc_html_e( 'Previous Post', 'weblogs' ); ?>
					</span>
					<span class="post-navigation__title">
						<?php echo esc_html( get_the_title( $prev->ID ) ); ?>
					</span>
				</div>
			</a>
		</div>
		<?php } ?>
		<?php if ( $next ) { ?>
		<div class="post-navigation__post post-navigation__post--next">
			<a class="post-navigation__link" href="<?php the_permalink( $next->ID ); ?>">
				<?php if ( has_post_thumbnail( $next->ID ) ) { ?>
				<div class="post-navigation__thumbnail">
					<?php echo get_the_post_thumbnail( $next->ID, 'thumbnail' ); ?>
				</div>
				<?php } ?>
				<div class="post-navigation__content">
					<span class="post-navigation__title">
						<?php esc_html_e( 'Next Post', 'weblogs' ); ?>
					</span>
					<span class="post-navigation__title">
						<?php echo esc_html( get_the_title( $next->ID ) ); ?>
					</span>
				</div>
			</a>
		</div>
		<?php } ?>
	</div>
</nav>
	<?php
}
