<?php
/**
 * The page content template part
 *
 * @package weblogs
 */

?>
<article <?php post_class(); ?>>
	<div class="page__content">
		<?php the_content(); ?>
	</div>
</article>
