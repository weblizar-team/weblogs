<?php
/**
 * The footer info template part
 *
 * @package weblogs
 */

$site_info = get_theme_mod( 'weblogs_footer_site_info', '' );
if ( $site_info ) {
	$allowed = array(
		'a' => array(
			'href'   => array(),
			'title'  => array(),
			'target' => array(),
			'class'  => array(),
		),
	);
	?>
	<div id="footer-site-info">
		<div class="site-info">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-md-12 site-info__text">
						<?php echo wp_kses( $site_info, $allowed ); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php
}
