<?php
/**
 * The comments template file
 *
 * @package weblogs
 */

if ( post_password_required() ) {
	return;
}
?>
<div id="comments" class="comments">
	<?php if ( have_comments() ) { ?>
		<div class="comments__title u-space-60">
			<h2>
				<?php
				printf(
					esc_html(
						/* translators: 1 is the number of comments and 2 is the post title */
						_n(
							'%1$s Reply to "%2$s"',
							'%1$s Replies to "%2$s"',
							get_comments_number(),
							'weblogs'
						)
					),
					esc_html( number_format_i18n( get_comments_number() ) ),
					esc_html( get_the_title() )
				);
				?>
			</h2>
		</div>
		<ul class="comments__list">
		<?php
			wp_list_comments(
				array(
					'style'       => 'li',
					'short_ping'  => true,
					'avatar_size' => 100,
					'reply_text'  => esc_html__( 'Reply', 'weblogs' ),
					'callback'    => 'weblogs_comment_callback',
				)
			);
		?>
		</ul>
		<?php the_comments_pagination(); ?>
	<?php } ?>

	<?php if ( ! comments_open() && get_comments_number() ) { ?>
		<p class="comments__closed"><?php esc_html_e( 'Comments are closed for this post.', 'weblogs' ); ?></p>
	<?php } ?>

	<?php comment_form(); ?>
</div>
