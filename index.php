<?php
/**
 * The main template file
 *
 * @package weblogs
 */

$sidebar_postion = get_theme_mod( 'weblogs_default_sidebar_position', false );
$sidebar_postion = weblogs_sanitize_default_sidebar_position( $sidebar_postion );

get_header();
?>

<div class="container u-space">
	<div class="row">
           
        
		<?php if ( 'left' === $sidebar_postion ) { ?>
			<?php if ( is_active_sidebar( 'primary-sidebar' ) ) { ?>
			<div class="col-xs-12 col-md-4">
				<?php get_sidebar(); ?>
			</div>
			<?php } ?>
		<?php } ?>

		<div class="col-xs-12 col-md-<?php echo is_active_sidebar( 'primary-sidebar' ) ? '8' : '12'; ?>">
			<main role="main">
			<?php get_template_part( 'loop', 'index' ); ?>
			</main>
		</div>

		<?php if ( 'right' === $sidebar_postion ) { ?>
			<?php if ( is_active_sidebar( 'primary-sidebar' ) ) { ?>
			<div class="col-xs-12 col-md-4">
				<?php get_sidebar(); ?>
			</div>
			<?php } ?>
		<?php } ?>
	</div>
</div>

<?php
get_footer();
?>