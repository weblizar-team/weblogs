<?php
/**
 * The author template file
 *
 * @package weblogs
 */

get_header();

$author_id          = get_query_var( 'author' );
$author_posts       = count_user_posts( $author_id );
$author_display     = get_the_author_meta( 'display_name', $author_id );
$author_description = get_the_author_meta( 'user_description', $author_id );
$author_website     = get_the_author_meta( 'user_url', $author_id );
?>

<div class="container page-author u-space">
	<div class="row">
		<div class="col-xs-12 col-md-4">
			<header class="page-author__header">
				<div class="page-author__pic"> <?php echo get_avatar( $author_id, 120 ); ?>  </div>
				<div class="page-author__info">
				<h1><strong><?php echo esc_html( $author_display ); ?></strong></h1>
				<p>
				<?php
				/* translators: %s: number of author's posts */
				printf( esc_html( _n( '%s post', '%s posts', $author_posts, 'weblogs' ) ), esc_html( number_format_i18n( $author_posts ) ) );
				?>
				</p>
				<?php if ( $author_website ) { ?>
				<a target="_blank" href="<?php echo esc_url( $author_website ); ?>">
					<?php echo esc_html( $author_website ); ?>
				</a>
				<?php } ?>
				</div>
			</header>
			<?php if ( $author_description ) { ?>
				<p class="post-author__desc"><?php echo esc_html( $author_description ); ?></p>
			<?php } ?>
		</div>
		<div class="col-xs-12 col-md-8">
			<main role="main">
			<?php get_template_part( 'loop', 'author' ); ?>
			</main>
		</div>
	</div>
</div>
<?php
get_footer();
