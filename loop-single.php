<?php
/**
 * The single loop template part
 *
 * @package weblogs
 */

if ( have_posts() ) {
	while ( have_posts() ) {
		the_post();
		get_template_part( 'template-parts/post/content', get_post_format() );
		if ( get_theme_mod( 'weblogs_show_author_details', true ) ) {
			get_template_part( 'template-parts/single/author' );
		}

		get_template_part( 'template-parts/single/navigation' );

		if ( comments_open() || get_comments_number() ) {
			comments_template();
		}
	}
} else {
	get_template_part( 'template-parts/post/content', 'none' );
}
