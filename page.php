<?php
/**
 * The page template file
 *
 * @package weblogs
 */

get_header();
?>

<header class="page__header">
	<div class="container">
		<h1 class="page__title">
			<?php the_title(); ?>
		</h1>
	</div>
</header>
<div class="container page-sidebar u-space">
	<div class="row">
		<div class="col-xs-12 col-md-8">
			<?php if ( '' !== get_the_post_thumbnail() ) { ?>
			<div class="page_thumbnail">
				<?php the_post_thumbnail( 'large' ); ?>
			</div>
			<?php } ?>
			<main role="main">
			<?php get_template_part( 'loop', 'page' ); ?>
			</main>
		</div>
		<div class="col-xs-12 col-md-4">
			<?php get_sidebar(); ?>
		</div>
	</div>
</div>
<?php
get_footer();
