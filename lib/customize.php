<?php
/**
 * Theme customizer
 *
 * @package weblogs
 */

/**
 * Register customizer
 *
 * @param WP_Customize $wp_customize The customize object.
 * @return void
 */
function weblogs_customize_register( $wp_customize ) {
	/* Get setting: blogname */
	$wp_customize->get_setting( 'blogname' )->transport = 'postMessage';

	/* Selective Refresh: blogname */
	$wp_customize->selective_refresh->add_partial(
		'blogname',
		array(
			'selector'            => '.header__blogname',
			'container_inclusive' => false,
			'render_callback'     => function() {
				echo esc_html( get_bloginfo( 'name' ) );
			},
		)
	);

	/* Get setting: blogdescription */
	$wp_customize->get_setting( 'blogdescription' )->transport = 'postMessage';

	/* Selective Refresh: blogdescription */
	$wp_customize->selective_refresh->add_partial(
		'blogdescription',
		array(
			'selector'            => '.header__description',
			'container_inclusive' => false,
			'render_callback'     => function() {
				echo esc_html( get_bloginfo( 'description' ) );
			},
		)
	);

	/* Section: General Options */
	$wp_customize->add_section(
		'weblogs_general_options',
		array(
			'title'       => esc_html__( 'General Options', 'weblogs' ),
			'description' => esc_html__( 'You can change general options from here.', 'weblogs' ),
			'priority'    => 155,
		)
	);

	/* Setting: Accent Color */
	$wp_customize->add_setting(
		'weblogs_accent_color',
		array(
			'default'           => '#2196F3',
			'sanitize_callback' => 'sanitize_hex_color',
			'transport'         => 'refresh',
		)
	);

	/* Control: Accent Color */
	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'weblogs_accent_color',
			array(
				'label'   => esc_html__( 'Accent Color', 'weblogs' ),
				'section' => 'weblogs_general_options',
			)
		)
	);

	/* Section: Footer Options */
	$wp_customize->add_section(
		'weblogs_footer_options',
		array(
			'title'       => esc_html__( 'Footer Options', 'weblogs' ),
			'description' => esc_html__( 'You can change footer options from here.', 'weblogs' ),
			'priority'    => 170,
		)
	);

	/* Setting: Footer Site Info */
	$wp_customize->add_setting(
		'weblogs_footer_site_info',
		array(
			'default'           => '',
			'sanitize_callback' => 'weblogs_sanitize_footer_site_info',
			'transport'         => 'postMessage',
		)
	);

	/* Control: Footer Site Info */
	$wp_customize->add_control(
		'weblogs_footer_site_info',
		array(
			'type'     => 'text',
			'label'    => esc_html__( 'Site Info', 'weblogs' ),
			'section'  => 'weblogs_footer_options',
		)
	);

	/* Selective Refresh: Footer Site Info */
	$wp_customize->selective_refresh->add_partial(
		'weblogs_footer_site_info',
		array(
			'selector'            => '#footer-site-info',
			'container_inclusive' => false,
			'render_callback'     => function() {
				get_template_part( 'template-parts/footer/info' );
			},
		)
	);

	/* Setting: Footer Layout */
	$wp_customize->add_setting(
		'weblogs_footer_layout',
		array(
			'default'           => '3,3,3,3',
			'sanitize_callback' => 'sanitize_text_field',
			'validate_callback' => 'weblogs_validate_footer_layout',
			'transport'         => 'postMessage',
		)
	);

	/* Control: Footer Layout */
	$wp_customize->add_control(
		'weblogs_footer_layout',
		array(
			'type'     => 'text',
			'label'    => esc_html__( 'Footer Layout', 'weblogs' ),
			'section'  => 'weblogs_footer_options',
		)
	);

	/* Selective Refresh: Footer Layout */
	$wp_customize->selective_refresh->add_partial(
		'weblogs_footer_layout',
		array(
			'selector'            => '#footer-widgets',
			'container_inclusive' => false,
			'render_callback'     => function() {
				get_template_part( 'template-parts/footer/widgets' );
			},
		)
	);

	/* Section: Blog Options */
	// $wp_customize->add_section(
	// 	'weblogs_blog_options',
	// 	array(
	// 		'title'       => esc_html__( 'Blog Options', 'weblogs' ),
	// 		'description' => esc_html__( 'You can change blog options from here.', 'weblogs' ),
	// 		'priority'    => 160,
	// 	)
	// );

	/* Setting: Default sidebar position */
	$wp_customize->add_setting(
		'weblogs_default_sidebar_position',
		array(
			'default'           => 'right',
			'sanitize_callback' => 'weblogs_sanitize_default_sidebar_position',
			'transport'         => 'refresh',
		)
	);

	/* Control: Default sidebar position */
	$wp_customize->add_control(
		'weblogs_default_sidebar_position',
		array(
			'type'     => 'radio',
			'label'    => esc_html__( 'Set default sidebar positon', 'weblogs' ),
			'section'  => 'weblogs_blog_options',
			'choices'  => array(
				'right' => esc_html__( 'Right Side', 'weblogs' ),
				'left'  => esc_html__( 'Left Side', 'weblogs' ),
			),
		)
	);

	/* Setting: Show post date in meta data */
	$wp_customize->add_setting(
		'weblogs_show_post_date',
		array(
			'default'           => true,
			'sanitize_callback' => 'weblogs_sanitize_checkbox',
			'transport'         => 'refresh',
		)
	);

	/* Control: Show post date in meta data */
	$wp_customize->add_control(
		'weblogs_show_post_date',
		array(
			'type'     => 'checkbox',
			'label'    => esc_html__( 'Show post date in meta data', 'weblogs' ),
			'section'  => 'weblogs_blog_options',
		)
	);

	/* Setting: Show post author in meta data */
	$wp_customize->add_setting(
		'weblogs_show_post_author',
		array(
			'default'           => true,
			'sanitize_callback' => 'weblogs_sanitize_checkbox',
			'transport'         => 'refresh',
		)
	);

	/* Control: Show post author in meta data */
	$wp_customize->add_control(
		'weblogs_show_post_author',
		array(
			'type'     => 'checkbox',
			'label'    => esc_html__( 'Show post author in meta data', 'weblogs' ),
			'section'  => 'weblogs_blog_options',
		)
	);

	/* Setting: Show Author Details */
	$wp_customize->add_setting(
		'weblogs_show_author_details',
		array(
			'default'           => true,
			'sanitize_callback' => 'weblogs_sanitize_checkbox',
			'transport'         => 'postMessage',
		)
	);

	/* Control: Show Author Details */
	$wp_customize->add_control(
		'weblogs_show_author_details',
		array(
			'type'     => 'checkbox',
			'label'    => esc_html__( 'Show author details in single blog post', 'weblogs' ),
			'section'  => 'weblogs_blog_options',
		)
	);
}
add_action( 'customize_register', 'weblogs_customize_register' );

/**
 * Sanitize site info
 *
 * @param string $input Input to be sanitized.
 * @return string
 */
function weblogs_sanitize_footer_site_info( $input ) {
	$allowed = array(
		'a' => array(
			'href'   => array(),
			'title'  => array(),
			'target' => array(),
		),
	);
	return wp_kses( $input, $allowed );
}

/**
 * Validate footer layout
 *
 * @param  Object $validity The validity of field.
 * @param  string $value Field to be validated.
 * @return Object
 */
function weblogs_validate_footer_layout( $validity, $value ) {
	if ( ! preg_match( '/^([1-9]|1[012])(,([1-9]|1[012]))*$/', $value ) ) {
		$validity->add( 'invalid_footer_layout', esc_html__( 'Footer layout is invalid.', 'weblogs' ) );
	}
	return $validity;
}

/**
 * Sanitize checkbox
 *
 * @param  boolean $checked The input value.
 * @return boolean
 */
function weblogs_sanitize_checkbox( $checked ) {
	return ( isset( $checked ) && true === $checked ) ? true : false;
}

/**
 * Sanitize default sidebar position
 *
 * @param  string $input The input value.
 * @return string
 */
function weblogs_sanitize_default_sidebar_position( $input ) {
	$valid = array( 'right', 'left' );
	if ( in_array( $input, $valid, true ) ) {
		return $input;
	}
	return 'right';
}
