<?php
/**
 * Enqueue theme assets
 *
 * @package weblogs
 */

/**
 * Enqueue assets for public
 *
 * @return void
 */
function weblogs_assets() {

	wp_enqueue_style( 'main-style', get_stylesheet_uri() );
	wp_enqueue_style( 'weblogs-bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css', array(), '4.3.1', 'all' );
	wp_enqueue_style( 'weblogs-font-awesome-free', get_template_directory_uri() . '/assets/css/all.min.css', array(), '5.8.1', 'all' );
	wp_enqueue_style( 'weblogs-style', get_template_directory_uri() . '/assets/css/style.css', array(), '1.0.0', 'all' );

	include get_template_directory() . '/lib/inline-style.php';
	wp_add_inline_style( 'weblogs-style', $inline_style );

	wp_enqueue_script( 'weblogs-popper', get_template_directory_uri() . '/assets/js/popper.min.js', array( 'jquery' ), true, true );
	wp_enqueue_script( 'weblogs-bootstrap-js', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array( 'popper' ), '4.3.1', true );
	wp_enqueue_script( 'weblogs-script', get_template_directory_uri() . '/assets/js/script.js', array( 'jquery' ), '1.0.0', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'weblogs_assets' );

/**
 * Enqueue assets for admin
 *
 * @return void
 */
function weblogs_admin_assets() {
	wp_enqueue_style( 'weblogs-admin', get_template_directory_uri() . '/assets/css/admin.css', array(), '1.0.0', 'all' );

	wp_enqueue_script( 'weblogs-admin', get_template_directory_uri() . '/assets/js/admin.js', array( 'jquery' ), '1.0.0', true );
}
add_action( 'admin_enqueue_scripts', 'weblogs_admin_assets' );

/**
 * Enqueue assets for customizer
 *
 * @return void
 */
function weblogs_customize_preview_js() {
	wp_enqueue_script( 'weblogs-customize-preview', get_template_directory_uri() . '/assets/js/customize-preview.js', array( 'customize-preview', 'jquery' ), '1.0.0', true );
}
add_action( 'customize_preview_init', 'weblogs_customize_preview_js' );

/**
 * Enqueue assets block editor
 *
 * @return void
 */
function weblogs_block_editor_assets() {
	wp_enqueue_style( 'weblogs-block-editor', get_template_directory_uri() . '/assets/css/style-editor.css', array(), '1.0.0', 'all' );
}
add_action( 'enqueue_block_editor_assets', 'weblogs_block_editor_assets' );
