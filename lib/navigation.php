<?php
/**
 * Navigation functions
 *
 * @package weblogs
 */

/**
 * Register menus
 *
 * @return void
 */
function weblogs_register_menus() {
	register_nav_menus(
		array(
			'main-menu' => esc_html__( 'Main Menu', 'weblogs' ),
		)
	);
}
add_action( 'init', 'weblogs_register_menus' );
