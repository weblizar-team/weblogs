<?php
/**
 * Include plugins for theme
 *
 * @package weblogs
 */

// TGM plugin activation.
require_once get_template_directory() . '/lib/class-tgm-plugin-activation.php';

/**
 * Register required plugins
 *
 * @return void
 */
function weblogs_register_required_plugins() {
	$plugins = array(
		array(
            'name'      => 'weblizar-companion',
            'slug'      => 'weblizar-companion',
            'required'  => true,
        )	
	);

	$config = array();

	tgmpa( $plugins, $config );
}
add_action( 'tgmpa_register', 'weblogs_register_required_plugins' );
