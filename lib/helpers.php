<?php
/**
 * Helper functions
 *
 * @package weblogs
 */

/**
 * Outputs the post meta
 *
 * @return void
 */
function weblogs_post_meta() {
	$show_post_date   = get_theme_mod( 'weblogs_show_post_date', true );
	$show_post_author = get_theme_mod( 'weblogs_show_post_author', true );

	if ( $show_post_date ) {
		/* translators: %s: Post Date */
		printf( esc_html__( 'Posted on %s', 'weblogs' ), '<a href="' . esc_attr( get_permalink() ) . '"><time datetime="' . esc_attr( get_the_date( 'c' ) ) . '">' . esc_html( get_the_date() ) . '</time></a>' );
	}

	if ( $show_post_author ) {
		/* translators: %s: Post Author */
		printf( esc_html__( ' By %s', 'weblogs' ), '<a href="' . esc_attr( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a>' );
	}
}

/**
 * Outputs the readmore link
 *
 * @return void
 */
function weblogs_readmore_link() {
	echo '<a class="post__readmore-link" href="' . esc_url( get_permalink() ) . '" title="' . the_title_attribute( array( 'echo' => false ) ) . '">';
	printf(
		wp_kses(
			/* translators: %s: Post Title */
			__( 'Read More <span class="screen-reader-text">About %s</span>', 'weblogs' ),
			array(
				'span' => array( 'class' => array() ),
			)
		),
		esc_html( get_the_title() )
	);
	echo '</a>';
}

/**
 * Get post meta
 *
 * @param  int          $id      The post ID.
 * @param  string       $key     The post meta key.
 * @param  array/string $default The default value.
 * @return array/string
 */
function weblogs_get_post_meta( $id, $key, $default ) {
	$value = get_post_meta( $id, $key, true );
	if ( ! $value && $default ) {
		return $default;
	}
	return $value;
}
