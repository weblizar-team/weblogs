<?php
/**
 * The comment callback function
 *
 * @package weblogs
 */

/**
 * Callback function to display the comments
 *
 * @param  object $comment Comment object.
 * @param  array  $args Arguments to this callback.
 * @param  int    $depth Depth of comment reply.
 * @return void
 */
function weblogs_comment_callback( $comment, $args, $depth ) {
	$tag = ( 'div' === $args['style'] ) ? 'div' : 'li';
	?>
	<<?php echo esc_attr( $tag ); ?> id="comment-<?php comment_ID(); ?>" <?php comment_class( array( 'comment', $comment->comment_parent ? 'comment--child' : '' ) ); ?>>
		<article id="div-comment-<?php comment_ID(); ?>" class="comment__body">
			<?php
			if ( 0 !== $args['avatar_size'] ) {
				echo get_avatar( $comment, $args['avatar_size'], false, false, array( 'class' => 'comment__avatar' ) );
			}
			?>
			<?php edit_comment_link( esc_html__( 'Edit Comment', 'weblogs' ), '<span class="comment__edit-link">', '</span>' ); ?>

			<div class="comment__content">
				<div class="comment__author">
					<?php echo get_comment_author_link( $comment ); ?>
				</div>

				<a class="comment__time" href="<?php echo esc_url( get_comment_link( $comment, $args ) ); ?>">
					<time datetime="<?php comment_time( 'c' ); ?>">
						<?php
						/* translators: %s: Time ago */
						printf( esc_html__( '%s ago', 'weblogs' ), esc_html( human_time_diff( get_comment_time( 'U' ), current_time( 'U' ) ) ) );
						?>
					</time>
				</a>

				<?php if ( '0' === $comment->comment_approved ) { ?>
				<p class="comment__awaiting-moderation"><?php esc_html_e( 'Your comment is awaiting moderation.', 'weblogs' ); ?></p>
				<?php } ?>

				<?php
				if ( 'comment' === $comment->comment_type || ( ( 'pingback' === $comment->comment_type || 'trackback' === $comment->comment_type ) && ! $args['short_ping'] ) ) {
					comment_text();
				}
				?>

				<?php
				comment_reply_link(
					array_merge(
						$args,
						array(
							'depth'      => $depth,
							'add_below'  => 'div-comment',
							'before'     => '<div class="comment__reply-link">',
							'after'      => '<div>',
						)
					)
				);
				?>
			</div>
		</article>
		<?php
}
