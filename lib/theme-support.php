<?php
/**
 * Theme features support
 *
 * @package weblogs
 */

/**
 * After setup theme callback
 *
 * @return void
 */
function weblogs_theme_support() {
	add_theme_support( 'title-tag' );
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'html5', array( 'search-form', 'comment-list', 'comment-form', 'gallery', 'caption' ) );
	add_theme_support( 'customize-selective-refresh-widgets' );

	/* Gutenberg */
	add_theme_support( 'wp-block-styles' );
	add_theme_support( 'align-wide' );

	/* Block Color Palettes */
	add_theme_support( 'editor-color-palette', array(
	    array(
	        'name' => __( 'strong magenta', 'weblogs' ),
	        'slug' => 'strong-magenta',
	        'color' => '#a156b4',
	    ),
	    array(
	        'name' => __( 'light grayish magenta', 'weblogs' ),
	        'slug' => 'light-grayish-magenta',
	        'color' => '#d0a5db',
	    ),
	    array(
	        'name' => __( 'very light gray', 'weblogs' ),
	        'slug' => 'very-light-gray',
	        'color' => '#eee',
	    ),
	    array(
	        'name' => __( 'very dark gray', 'weblogs' ),
	        'slug' => 'very-dark-gray',
	        'color' => '#444',
	    ),
	) );

	/* Block Font Sizes */
	add_theme_support( 'editor-font-sizes', array(
	    array(
	        'name' => __( 'small', 'weblogs' ),
	        'shortName' => __( 'S', 'weblogs' ),
	        'size' => 12,
	        'slug' => 'small'
	    ),
	    array(
	        'name' => __( 'regular', 'weblogs' ),
	        'shortName' => __( 'M', 'weblogs' ),
	        'size' => 16,
	        'slug' => 'regular'
	    ),
	    array(
	        'name' => __( 'large', 'weblogs' ),
	        'shortName' => __( 'L', 'weblogs' ),
	        'size' => 36,
	        'slug' => 'large'
	    ),
	    array(
	        'name' => __( 'larger', 'weblogs' ),
	        'shortName' => __( 'XL', 'weblogs' ),
	        'size' => 50,
	        'slug' => 'larger'
	    )
	) );

	/* Disabling custom colors in block Color Palettes */
	add_theme_support( 'disable-custom-colors' );

	/* Load editor style. */
	add_editor_style();

	/* Enable support for Woocommerce */
  	add_theme_support( 'woocommerce' );
  	add_theme_support( 'wc-product-gallery-zoom' );
    add_theme_support( 'wc-product-gallery-lightbox' );
    add_theme_support( 'wc-product-gallery-slider' );

    /* Allow shortcodes in widgets. */
	add_filter( 'widget_text', 'do_shortcode' );

	add_theme_support(
		'custom-logo',
		array(
			'height'      => 100,
			'width'       => 100,
			'flex-height' => true,
			'flex-width'  => true,
		)
	);

	$defaults = array(
    'default-image'          => '',
    'flex-width'             => true,
	'width'                  => 980,
	'flex-height'            => true,
	'height'                 => 400,
    'uploads'                => true,
    'random-default'         => true,
    'header-text'            => true,
    'default-text-color'     => '#fff',
    'wp-head-callback'       => '',
    'admin-head-callback'    => '',
    'admin-preview-callback' => '',
    );
  	add_theme_support( 'custom-header', $defaults );

  	add_theme_support( 'custom-background' );

	add_theme_support( 'align-wide' );
	add_theme_support( 'editor-styles' );
	add_theme_support( 'automatic-feed-links' );
	add_theme_support(
		'post-formats',
		array(
			'aside',
			'image',
			'video',
			'quote',
			'link',
			'audio',
		)
	);
}
add_action( 'after_setup_theme', 'weblogs_theme_support' );
