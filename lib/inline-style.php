<?php
/**
 * Inline style variable
 *
 * @package weblogs
 */

$accent_color = sanitize_hex_color( get_theme_mod( 'weblogs_accent_color', '#2196F3' ) );

$inline_style = "
a,
a:link,
a:visited {
	color: {$accent_color};
}
.main-header {
	background-color: {$accent_color};
}
.dropdown-item.active, .dropdown-item:active {
	background-color: {$accent_color};
}
.page-404 .search-form__button, .search-page .search-form__button {
	background-color: {$accent_color};
}
.footer-widget h5 {
	border-left-color: {$accent_color};
}
.footer li a:after {
	background-color: {$accent_color};
}
#wp-calendar thead {
	background: {$accent_color};
}
.primary-sidebar .sidebar-widget .search-form__button:hover {
	background-color: {$accent_color};
}
.primary-sidebar .widget_archive li a:after {
	background-color: {$accent_color};
}
.primary-sidebar ul li a:after {
	background-color: {$accent_color};
}
.sticky {
	border-left-color: {$accent_color};
}
.post__tags li a,
main .page__tags li a {
	background: {$accent_color};
}
.post__tags li a:after,
main .page__tags li a:after {
	background-color: {$accent_color};
}
.post .post__posted_in,
main .page .post__posted_in {
	background: {$accent_color};
}
.post .post__posted_in:after,
main .page .post__posted_in:after {
	border-left-color: {$accent_color};
}
.post .post__cats a:hover,
main .page .post__cats a:hover {
	background: {$accent_color} !important;
}
.post__readmore-link,
main .page__readmore-link {
	background: {$accent_color};
}
.post-author {
	background-color: {$accent_color};
}
.post-navigation a:hover {
	color: {$accent_color};
}
a.comment-edit-link:hover {
	color: {$accent_color} !important;
}
.bypostauthor>.comment__body .comment__content {
	background-color: {$accent_color};
}
.bypostauthor>.comment__body .comment__content::after {
	border-left: 2em solid {$accent_color} !important;
}
.pagination .page-numbers:hover {
	background: {$accent_color} !important;
}
.pagination span.page-numbers.current {
	background: {$accent_color} !important;
}
#commentform input[type=submit] {
	background: {$accent_color};
}
.wp-block-column {
	border-color: {$accent_color};
}
.post-password-form input[type='submit'] {
	background: {$accent_color};
}
.tagcloud a:hover {
	background: {$accent_color};
}
.post__single-title:first-letter,
.post__title:first-letter,
.sidebar-widget h5:first-letter {
	color: {$accent_color};
	border-bottom-color: {$accent_color};
}
";
