<?php
/**
 * Function to register sidebars
 *
 * @package weblogs
 */

/**
 * Register sidebar widgets
 *
 * @return void
 */
function weblogs_sidebar_widgets() {
	register_sidebar(
		array(
			'id'            => 'primary-sidebar',
			'name'          => esc_html__( 'Primary Sidebar', 'weblogs' ),
			'description'   => esc_html__( 'This sidebar appears in the blog post or page', 'weblogs' ),
			'before_widget' => '<section id="%1$s" class="sidebar-widget mb-2 %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h5>',
			'after_title'   => '</h5>',
		)
	);

	$footer_layout = '3,3,3,3';
	$footer_layout = preg_replace( '/\s+/', '', $footer_layout );
	$columns       = explode( ',', $footer_layout );

	foreach ( $columns as $i => $column ) {
		register_sidebar(
			array(
				'id'            => 'footer-sidebar-' . ( $i + 1 ),
				/* translators: %s: Footer widget column number */
				'name'          => sprintf( esc_html__( 'Footer Widgets Column %s', 'weblogs' ), $i + 1 ),
				'description'   => esc_html__( 'Footer Widgets', 'weblogs' ),
				'before_widget' => '<section id="%1$s" class="footer-widget %2$s">',
				'after_widget'  => '</section>',
				'before_title'  => '<h5>',
				'after_title'   => '</h5>',
			)
		);
	}
}
add_action( 'widgets_init', 'weblogs_sidebar_widgets' );
