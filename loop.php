<?php
/**
 * The loop template part
 *
 * @package weblogs
 */

if ( have_posts() ) {
	while ( have_posts() ) {
		the_post();
		get_template_part( 'template-parts/post/content', get_post_format() );
	}
	the_posts_pagination();
} else {
	get_template_part( 'template-parts/post/content', 'none' );
}
