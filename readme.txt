=== weblogs ===
Contributors: weblizar
Tags: two-columns, custom-menu, right-sidebar, custom-background, featured-image-header, sticky-post, threaded-comments, featured-images, blog.
Requires at least: 4.0
Tested up to: 5.2
Stable tag:  1.0
License: GNU General Public License v3.0 or later
License URI: https://www.gnu.org/licenses/gpl-3.0.html

Free WordPress theme can be used for Simple Realistic Blogging Sites.

== Description ==
weblogs is responsive , multi-page WordPress theme for Business. Theme is built with leading CSS framework which adapts all leading devices , browsers and Page Builders. This theme is compatible with all leading browsers and all popular wordpress plugins like WPML / Elemantor / contact form 7 etc.

== Resources ==

weblogs WordPress Theme, Copyright 2019 weblizar
weblogs is distributed under the terms of the GNU GPL.

Theme is Built using the following resource bundles.                     

= BUNDELED JS = 

* Bootstrap jQuery library @ 2011-2019 Licensed under MIT.
* Popper @ 2019, Federico Zivolo, http://opensource.org/licenses/MIT). MIT License
* Custom script @ 2019, Weblizar, GPL
* Customozer script @ 2019, Weblizar, GPL

= BUNDELED CSS =

* Font Awesome Free 5.8.1 by @fontawesome - https://fontawesome.com , MIT License
* Bootstrap CSS, @ 2019 Twitter, MIT License
* main_style CSS, @ 2019, weblizar, GPL 
* Editor style CSS, @ 2019, weblizar, GPL 