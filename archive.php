<?php
/**
 * The archive template file
 *
 * @package weblogs
 */

$sidebar_postion = get_theme_mod( 'weblogs_default_sidebar_position', false );
$sidebar_postion = weblogs_sanitize_default_sidebar_position( $sidebar_postion );

get_header();
?>
		<header class="page__header archive-page__header">
			<?php the_archive_title( '<h1>', '</h1>' ); ?>
			<?php the_archive_description( '<p>', '</p>' ); ?>
		</header>

<div class="container page-archive u-space">
	<div class="row">
		<?php if ( 'left' === $sidebar_postion ) { ?>
			<?php if ( is_active_sidebar( 'primary-sidebar' ) ) { ?>
			<div class="col-xs-12 col-md-4">
				<?php get_sidebar(); ?>
			</div>
			<?php } ?>
		<?php } ?>

		<div class="col-xs-12 col-md-<?php echo is_active_sidebar( 'primary-sidebar' ) ? '8' : '12'; ?>">
			<main role="main">
			<?php get_template_part( 'loop', 'archive' ); ?>
			</main>
		</div>

		<?php if ( 'right' === $sidebar_postion ) { ?>
			<?php if ( is_active_sidebar( 'primary-sidebar' ) ) { ?>
			<div class="col-xs-12 col-md-4">
				<?php get_sidebar(); ?>
			</div>
			<?php } ?>
		<?php } ?>
</div>
<?php
get_footer();
